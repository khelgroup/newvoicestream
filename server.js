// http://127.0.0.1:9001
// http://localhost:9001

const server = require('http'),
    url = require('url'),
    path = require('path'),
    fs = require('fs');

var ioServer = require('socket.io');
var RTCMultiConnectionServer = require('rtcmulticonnection-server');

var config = {
    "socketURL": "/",
    "dirPath": "",
    "homePage": "/",
    "socketMessageEvent": "RTCMultiConnection-Message",
    "socketCustomEvent": "RTCMultiConnection-Custom-Message",
    "port": 3000,
    "enableLogs": false,
    "isUseHTTPs": false,
    "enableAdmin": false
};

function serverHandler(request, response) {
    var uri = url.parse(request.url).pathname,
        filename = path.join(process.cwd(), uri);

    console.log('filename', filename);

    fs.exists(filename, function(exists) {
        if (!exists) {
            // console.log('1');
            response.writeHead(404, {
                'Content-Type': 'text/plain'
            });
            response.write('404 Not Found: ' + filename + '\n');
            response.end();
            return;
        }

        if (filename.indexOf('favicon.ico') !== -1) {
            // console.log('2');
            return;
        }

        // console.log('3');
        var isWin = !!process.platform.match(/^win/);

        if (fs.statSync(filename).isDirectory() && !isWin) {
            // console.log('4');
            filename += '/index.html';
        } else if (fs.statSync(filename).isDirectory() && !!isWin) {
            // console.log('5');
            filename += '\\index.html';
        }

        fs.readFile(filename, 'binary', function(err, file) {
            // console.log('6');
            if (err) {
                // console.log('7');
                response.writeHead(500, {
                    'Content-Type': 'text/plain'
                });
                response.write(err + '\n');
                response.end();
                return;
            }

            var contentType;

            if (filename.indexOf('.html') !== -1) {
                // console.log('8');
                contentType = 'text/html';
            }

            if (filename.indexOf('.js') !== -1) {
                // console.log('9');
                contentType = 'application/javascript';
            }

            if (contentType) {
                // console.log('10');
                response.writeHead(200, {
                    'Content-Type': contentType
                });
            } else {
                // console.log('11');
                response.writeHead(200);
            }

            response.write(file, 'binary');
            response.end();
        });
    });
}

var app = server.createServer(serverHandler);

RTCMultiConnectionServer.beforeHttpListen(app, config);

app = app.listen(process.env.PORT || 3000, process.env.IP || "0.0.0.0", function() {
    console.log('12');
    RTCMultiConnectionServer.afterHttpListen(app, config);
});

// --------------------------
// socket.io codes goes below

ioServer(app).on('connection', function(socket) {
    // console.log('13');
    RTCMultiConnectionServer.addSocket(socket, config);

    // ----------------------
    // below code is optional

    const params = socket.handshake.query;
    console.log('16', params);
    console.log('16', params.socketCustomEvent);

    if (!params.socketCustomEvent) {
        // console.log('14');
        params.socketCustomEvent = 'custom-message';
    }

    socket.on(params.socketCustomEvent, function(message) {
        console.log('15 function is called ON event and ready to emit');
        socket.broadcast.emit(params.socketCustomEvent, message);

    });
});
